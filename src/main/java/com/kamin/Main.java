package com.kamin;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.file.Paths;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        String log_path = "logs.txt";
        System.out.println("Старт программы контроля доступа.");
        Scanner scan = new Scanner(System.in);
        EmployeesRepo repo = new EmployeesRepo();
        String good = "Доступ разрешен. Сотрудник с id %d открыл дверь номер %d\n";
        String bad = "Доступ запрещен. Сотрудник с id %d пытался открыть дверь номер %d\n";
        String menu = """
                Введите номер команды:
                1.Создание нового сотрудника
                2.Удаление сотрудника
                3.Открытие двери
                4.Просмотр журнала доступа
                5.Выход""";
        while(true){
            System.out.println(menu);
            try {
                int number = scan.nextInt();
                switch (number){
                    case 1 ->{
                        System.out.println("Введите имя:");
                        String name = scan.next();
                        System.out.println("Введите должность:");
                        String position = scan.next();
                        System.out.println("Введите название отдела:");
                        String departure = scan.next();
                        System.out.println("Введите список дверей, которые может открывать пользователь через запятую, без пробелов:");
                        String doors = scan.next();
                        var door = Util.getDoors(doors);
                        repo.createEmployee(name,
                                position,
                                departure,
                                door);
                        System.out.println("Успешно создано.");
                    }
                    case 2 ->{
                        System.out.println("Введите идентификатор сотрудника, которого требуется удалить");
                        long id = scan.nextLong();
                        if(repo.deleteEmployee(id)){
                            System.out.println("Сотрудник успешно удален");
                        }else{
                            System.out.println("Сотрудника удалить не удалось");
                        }
                    }
                    case 3 ->{
                        System.out.println("Введите id сотрудника");
                        long id = scan.nextLong();
                        var empl = repo.getEmplById(id);
                        if(empl != null){
                            System.out.println("Введите номер двери");
                            int door_id = scan.nextInt();
                            if(empl.checkDoor(door_id)){
                                System.out.println("Доступ разрешен");
                                Writer.appendToFile(log_path,String.format(good, empl.getId(), door_id));
                            }else{
                                System.out.println("Доступ запрещен");
                                Writer.appendToFile(log_path,String.format(bad, empl.getId(), door_id));
                            }
                        }else{
                            System.out.println("Сотрудник не найден");
                        }
                    }
                    case 4 ->{
                        File file = new File(log_path);
                        if(file.exists()){
                            String line;
                            BufferedReader fin = new BufferedReader(new FileReader(file));
                            while ((line = fin.readLine()) != null) System.out.println(line);
                        }else{
                            System.out.println("Логов нет. Зайдите в следующий раз");
                        }
                    }
                    case 5 ->{
                        System.exit(0);
                    }
                }
            }catch(Exception e){
                System.out.println("Что-то пошло не так, " +
                        "попробуйте снова");
            }
        }
    }
}