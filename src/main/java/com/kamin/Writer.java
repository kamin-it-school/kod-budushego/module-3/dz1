package com.kamin;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

public class Writer {
    public static void appendToFile(String path,String data) throws IOException {
        var file = new File(path);
        var stream = new FileOutputStream(file,true);
        stream.write(data.getBytes(StandardCharsets.UTF_8));
        stream.close();
    }

    public static void saveToFile(String path,String data) throws IOException {
        var file = new File(path);
        var stream = new FileOutputStream(file,false);
        stream.write(data.getBytes(StandardCharsets.UTF_8));
        stream.close();
    }
}
