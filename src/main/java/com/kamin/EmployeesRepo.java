package com.kamin;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.*;

import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

@Getter
@Setter
public class EmployeesRepo {
    ObjectMapper objectMapper = new ObjectMapper();
    private ArrayList<Employee> employees;

    private final String path = "employers.txt";

    public EmployeesRepo(){
        File file = new File(path);
        try{
            employees = objectMapper.readValue(file,
                    new TypeReference<ArrayList<Employee>>(){});
        }catch (Exception e){
            System.out.println("File is invalid!");
            employees = new ArrayList<>();
        }
    }
    public void createEmployee(String name,
                               String position,
                               String departure,
                               Integer... doors){
        employees.add(new Employee(System.currentTimeMillis(),
                name,
                position,
                departure,
                new ArrayList<>(Arrays.asList(doors))));

        try{
            String json = objectMapper.writeValueAsString(employees);
            Writer.saveToFile(path,json);
        }catch(Exception e){
            System.out.println("Error! - " + e.getMessage());
        }

    }

    public boolean deleteEmployee(long id){
        boolean deleted = false;
        for (int i = 0; i < employees.size(); i++) {
            if(employees.get(i).getId() == id){
                employees.remove(employees.get(i));
                deleted = true;
                break;
            }
        }
        if(deleted) {
            try {
                String json = objectMapper.writeValueAsString(employees);
                Writer.saveToFile(path, json);
            } catch (Exception e) {
                System.out.println("Error! - " + e.getMessage());
            }
        }
        return deleted;
    }

    public Employee getEmplById(long id){
        for (Employee employee : employees) {
            if (employee.getId() == id) {
                return employee;
            }
        }
        return null;
    }
}
