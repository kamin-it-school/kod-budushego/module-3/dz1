package com.kamin;

import java.util.Arrays;

public class Util {
    public static Integer[] getDoors(String doors){
        String[] door = doors.split(",");
        return Arrays.stream(door).map(Integer::parseInt).toArray(Integer[]::new);
    }
}
