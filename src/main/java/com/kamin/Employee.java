package com.kamin;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.ArrayList;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class Employee {
    private Long id;
    private String name;
    private String position;
    private String departure;
    private ArrayList<Integer> doors;

    public boolean checkDoor(int id){
        return doors.contains(id);
    }
}
