import com.kamin.Util;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UtilTest {
    @Test
    void checkParseString(){
        String doors = "1,2,3,4,5";
        Integer[] door_exp = {1,2,3,4,5};
        var door = Util.getDoors(doors);
        Assertions.assertArrayEquals(door_exp,door);
    }

    @Test
    void checkParseStringBackSpace(){
        String doors = "1,2,3,4,  5";
        Integer[] door_exp = {1,2,3,4,5};
        Assertions.assertThrows(NumberFormatException.class,() -> Util.getDoors(doors));
    }

    @Test
    void checkParseStringException(){
        String doors = "1,2,3,4a,  5";
        Integer[] door_exp = {1,2,3,4,5};
        Assertions.assertThrows(NumberFormatException.class,() -> Util.getDoors(doors));
    }
}
