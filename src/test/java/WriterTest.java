import com.kamin.Writer;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class WriterTest {

    @Test
    void testAppendToFile() {
        Assertions.assertDoesNotThrow(() -> Writer.
                appendToFile("src/test/java/data.txt",
                        "test"));
    }
}
