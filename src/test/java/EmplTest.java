import com.kamin.Employee;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

public class EmplTest {
    @Test
    void checkDoorContains(){
        Integer[] doors = {1,2,3,4,5};
        var empl = new Employee(
                System.currentTimeMillis(),
                "test",
                "test",
                "test",
                new ArrayList<>(Arrays.asList(doors)));
        Assertions.assertTrue(empl.checkDoor(1));
    }

    @Test
    void checkDoorNoContains(){
        Integer[] doors = {1,2,3,4,5};
        var empl = new Employee(
                System.currentTimeMillis(),
                "test",
                "test",
                "test",
                new ArrayList<>(Arrays.asList(doors)));
        Assertions.assertFalse(empl.checkDoor(10));
    }
}
