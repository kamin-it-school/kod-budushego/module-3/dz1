import com.kamin.EmployeesRepo;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class EplRepoTest {

    @Test
    void createEmpty(){
        var repo = new EmployeesRepo();
        Assertions.assertEquals(0,repo.getEmployees().size());
    }

    @Test
    void createTest(){
        var repo = new EmployeesRepo();
        repo.createEmployee("test",
                "test",
                "test",
                1, 2, 3, 4, 5);
    }

    @Test
    void createArrayTest(){
        var repo = new EmployeesRepo();
        Integer[] doors = {1,2,3,4,5};
        repo.createEmployee("test",
                "test",
                "test",
                doors);
    }

    @Test
    void deleteTest(){
        var repo = new EmployeesRepo();
        repo.createEmployee("test",
                "test",
                "test",
                1, 2, 3, 4, 5);
        repo.createEmployee("test2",
                "test2",
                "test2",
                1, 2, 3, 4, 5);
        repo.createEmployee("test3",
                "test3",
                "test3",
                1, 2, 3, 4, 5);

        repo.deleteEmployee(repo.getEmployees().get(1).getId());
    }

    @Test
    void createNotEmpty(){
        var repo = new EmployeesRepo();
        Assertions.assertEquals(3,repo.getEmployees().size());
    }
}
